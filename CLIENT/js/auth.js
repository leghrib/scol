function login() {
  var settings = {
    async: true,
    crossDomain: true,
    url: baseUrl + "gs/ws/login/login.php",
    method: 'POST'
  };
  settings.data = {
    check: true
  };
  $.post(settings).done(function (response) {
    window.location.replace(baseUrl + "client/index.html");
  });
  $('#formLogin').submit(function (e) {
    e.preventDefault();
    var $form = $(this);
    var username = $form.find('input[name=\'username\']').val();
    var password = $form.find('input[name=\'password\']').val();
    settings.data = {
      username: username,
      password: password
    };
    console.log(settings);
    $.post(settings).done(function (response) {
      window.location.replace(baseUrl + "client/index.html");
    }).fail(function (response) {
      alert(response);
      console.log(response);
    });
  });
}

function logout() {
  console.log('logout');
  var settings = {
    async: true,
    crossDomain: true,
    url: baseUrl + "gs/ws/login/login.php",
    method: 'POST'
  };
  settings.data = {
    logout: true
  };
  $.post(settings).done(function (response) {
    console.log(response);
    window.location.replace(baseUrl + "client/login.html");
  });
}

function checkLogin() {
  var settings = {
    async: true,
    crossDomain: true,
    url: baseUrl + "gs/ws/login/login.php",
    method: 'POST',
    data: {
      check: true
    }
  };
  $.post(settings).done(function (response) {
    console.log(response);
    settings.url = baseUrl + "gs/ws/prof/prof.php";
    $.post(settings).done(function (response) {
      var prof = response;
      var fullname = prof.nom + " " + prof.prenom;
      $('#fullname').text(fullname);
    }).fail(function (response) {
      console.log(response.message);
    });
  }).fail(function (response) {
    window.location.replace(baseUrl + "client/login.html?msg=" + response.message);
  });
}
//# sourceMappingURL=auth.js.map