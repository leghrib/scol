baseUrl = "http://" + window.location.hostname + "/";

function initialSetup() {
  $.ajax({
    url: baseUrl,
    type: 'HEAD',
    timeout: 1000,
    statusCode: {
      400: function _() {
        baseUrl = 'http://127.0.0.1/';
      },
      0: function _() {
        baseUrl = 'http://127.0.0.1/';
      }
    }
  });

  switch (page) {
    default:
      checkLogin();
      setupPage();
      break;

    case 'login':
      login();
      break;
  }
}

function setupPage() {
  if (page === 'marks') {
    marksPage();
  }

  if (page === 'marks.fill') {
    marksFillPage();
  }

  $('#logout').click(function () {
    logout();
  });

  function marksPage() {
    function moduleClassTag(spec, mod, classe, title) {
      return "<div class=\"ModuleClass\">\n      <div class=\"Description\">\n        <h5 class=\"Title\">\n          " + title + "\n        </h5>\n      </div>\n\n      <div class=\"Caption\">\n        <ul>\n          <li>\n            <a href=\"/client/marks-fill.html?title=" + title + "&classe=" + classe + "&mod=" + mod + "&trimester=1\">\u0627\u0644\u062B\u0644\u0627\u062B\u064A \u0627\u0644\u0623\u0648\u0644</a>\n          </li>\n          <li>\n            <a href=\"/client/marks-fill.html?title=" + title + "&classe=" + classe + "&mod=" + mod + "&trimester=2\">\u0627\u0644\u062B\u0644\u0627\u062B\u064A \u0627\u0644\u062B\u0627\u0646\u064A</a>\n          </li>\n          <li>\n            <a href=\"/client/marks-fill.html?title=" + title + "&classe=" + classe + "&mod=" + mod + "&trimester=3\">\u0627\u0644\u062B\u0644\u0627\u062B\u064A \u0627\u0644\u062B\u0627\u0644\u062B</a>\n          </li>\n        </ul>\n      </div>\n      </div>";
    }

    $.get(baseUrl + "GS/ws/prof/prof_mod.php").done(function (result) {
      window.result = result;
      var modulesTags = '';

      for (var i = 0; i < result.length; i++) {
        var moduleClass = result[i];
        modulesTags += moduleClassTag(moduleClass.spec, moduleClass.id_mod, moduleClass.classe, moduleClass.des_spec + " " + moduleClass.abr_spec + " " + moduleClass.classe);
      }

      var html = "<div class=\"Modules\">\n       " + modulesTags + "\n        </div>";
      $('div.marks').html(html);
    }).fail(function (error) {
      console.log(error);
    });
  }

  function marksFillPage() {
    setTimeout(function () {
      return $('.sidebar-minimizer').click();
    }, 1000);
    /* {
        classname: 'my-class',
        id: 'my-id',
        target: $('load-bar') }*/
    // const loadBar = new Nanobar({ target: $('#load-bar')[0] })

    var title = getUrlParameter('title');
    var classe = getUrlParameter('classe');
    var mod = getUrlParameter('mod');
    var trimester = getUrlParameter('trimester') ? getUrlParameter('trimester') : 1;
    $('.card-header').html("<i class=\"icon-pencil\" style=\"color: #3FBFBF;\"/>  \n      \u0645\u0644\u0623 \u0627\u0644\u0646\u0642\u0627\u0637 \u0644\u0644\u0642\u0633\u0645: <span class=\"badge badge-info\">" + title + "</span> \n      \u0644\u0644\u062B\u0644\u0627\u062B\u064A: <span class=\"badge badge-info\">" + trimester + "</span>");
    $.get(baseUrl + "GS/ws/note/note_class_mod.php", {
      mod: mod,
      classe: classe
    }).done(function (result) {
      var tabledata = result.map(function (el) {
        return {
          id: el.id_etud,
          name: el.nom + " " + el.prenom,
          dob: el.dt_nais.split(' ')[0],
          tkNote: el["ncc" + trimester],
          testNote: el["ndev" + trimester],
          examNote: el["ndev2" + trimester],
          projectsNote: el["prj" + trimester],
          readingNote: el["lect" + trimester],
          observation: el["obs" + trimester]
        };
      });
      console.log(result);
      $.get(baseUrl + "gs/ws/obs/obs.php?numObs=2").done(function (result) {
        var observations = {};
        result.forEach(function (obs) {
          observations["" + obs.id_obs] = obs.obs;
        });
        window.obs = observations;
        buildTable(tabledata, observations);
      });
    }).fail(function (error) {
      console.log(error);
    });

    function buildTable() {
      var tabledata = arguments.length <= 0 ? undefined : arguments[0];
      var observations = arguments.length <= 1 ? undefined : arguments[1];
      var studentsTable = new Tabulator('#marks-fill-table', {
        columnVertAlign: 'center',
        // align header contents to center of cell
        layout: 'fitDataFill',
        // persistentLayout: true,
        // persistentSort: true,
        // persistenceID: 'studentsTable',
        data: tabledata,
        // assign data to table
        cellEdited: onCellEdited,
        // validationFailed: onValidationFailed,
        columns: [// Define Table Columns
        {
          // create column group
          title: 'معلومات التلميذ',
          columns: [{
            title: 'الاسم واللقب',
            field: 'name',
            sorter: 'string'
          }, {
            title: 'تاريخ الميلاد',
            field: 'dob'
          }]
        }, {
          // create column group
          title: 'النقاط المحصلة',
          columns: [{
            title: 'ت.م / تعبير .ك',
            field: 'tkNote',
            sorter: 'number',
            editor: 'number',
            validator: ['min:0', 'max:20', 'numeric']
          }, {
            title: 'معدل الفروض',
            field: 'testNote',
            sorter: 'number',
            editor: 'number',
            editorParams: {
              min: 0,
              max: 20
            },
            validator: ['min:0', 'max:20', 'numeric']
          }, {
            title: 'الاختبار',
            field: 'examNote',
            sorter: 'number',
            editor: 'number',
            editorParams: {
              min: 0,
              max: 20
            },
            validator: ['min:0', 'max:20', 'numeric']
          }, {
            title: 'تثمين مشاريع',
            field: 'projectsNote',
            sorter: 'number',
            editor: 'number',
            editorParams: {
              min: 0,
              max: 20
            },
            validator: ['min:0', 'max:20', 'numeric']
          }, {
            title: 'تثمين مطالعة',
            field: 'readingNote',
            sorter: 'number',
            editor: 'number',
            editorParams: {
              min: 0,
              max: 20
            },
            validator: ['min:0', 'max:20', 'numeric']
          }]
        }, {
          title: 'ملاحظة',
          field: 'observation',
          editor: 'select',
          editorParams: {
            values: observations
          }
        }]
      });
      return studentsTable;
    }

    function onValidationFailed(cell, value, validators) {}

    function onCellEdited(cell) {
      // row - row component
      // loadBar.go(30)
      if (!cell) {
        return;
      }

      var note;
      var noteValue;

      switch (cell.getColumn().getField()) {
        case 'tkNote':
          note = "ncc" + trimester;
          break;

        case 'testNote':
          note = "ndev" + trimester;
          break;

        case 'examNote':
          note = "ndev2" + trimester;
          break;

        case 'projectsNote':
          note = "prj" + trimester;
          break;

        case 'readingNote':
          note = "lect" + trimester;
          break;

        case 'observation':
          note = "obs" + trimester;
          break;

        default:
          break;
      }

      if (cell.getValue() === cell.getOldValue()) {
        return;
      }

      noteValue = cell.getValue() ? cell.getValue() : '';
      var etud = cell.getData().id;
      var updatedData = {
        mod: mod,
        etud: etud,
        note: note,
        noteValue: noteValue
      };
      $.get(baseUrl + "GS/ws/note/update_note.php", updatedData).done(function (result) {}).fail(function (error) {
        alert(error);
      });
    }
  }
}

initialSetup();
//# sourceMappingURL=main.js.map