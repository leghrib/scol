<?php
/**
 * Created by PhpStorm.
 * User: Leghrib
 * Date: 29/06/2016
 * Time: 01:28
 */

require  __DIR__ . "/../config.php";
require_once __DIR__ . "/../db/connectionDB.php";
require_once __DIR__."/APP_UTILS.php";
require_once __DIR__."/PPPP.php";
require_once __DIR__ . "/../utils/UTILS.php";
require_once __DIR__ . "/../utils/auth.php";
require_once __DIR__ . "/../lib/firebase-php-jwt/index.php";
