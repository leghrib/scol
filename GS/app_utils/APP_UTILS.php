<?php

/**
 * Created by PhpStorm.
 * User: Leghrib
 * Date: 02/07/2016
 * Time: 14:33
 */
class APP_UTILS
{


    public static function verifyIsConnected()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        } else {

            header("location: ../login/deconnexion.php");
        }
    }

    public static function verifyIsConnectedAs($typeUtilisateur)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

            if ($_SESSION['utilisateur']->type != $typeUtilisateur) {
                APP_UTILS::redirectAsRole($_SESSION['utilisateur']->type);
            }

        } else {

            header("location: ../login/deconnexion.php");
        }
    }

    public static function verifyIsNotConnectedAs()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

            APP_UTILS::redirectAsRole($_SESSION['utilisateur']->type);


        }
    }

    public static function redirectAsRole($typeUtilisateur)
    {
        if ($typeUtilisateur == 1) {
            header("location: ../statistique/detailProfile.php");
            exit;
        } else if ($typeUtilisateur == 2) {
            header("location: ../Prof/detailProfileProf.php");
            exit;

        }

    }

    public static function isConnected()
    {

        session_start();

        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
            return true;
        } else {

            return false;
        }
    }



    public static function jour($jour)
    {
        if ($jour == 1) {
            return "السبت";
        } else if ($jour == 2) {
            return "الأحد";
        } else if ($jour == 3) {
            return "الاثنين";
        } else if ($jour == 4) {
            return "الثلاثاء";
        } else if ($jour == 5) {
            return "الاربعاء";
        } else if ($jour == 6) {
            return "الخميس";
        } else if ($jour == 7) {
            return "الجمعة";
        }
    }
    public static function getLieu($lieu)
    {
        if ($lieu == 1) {
            return "ورشة";
        } else if ($lieu == 2) {
            return "مخبر";
        } else if ($lieu == 3) {
            return "حجرة";
        } else if ($lieu == 4) {
            return "قاعة مجهزة";
        } else if ($lieu == 5) {
            return "قاعة رياضة";
        }
    }

}