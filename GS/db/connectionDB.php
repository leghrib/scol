<?php

/**
 * Created by PhpStorm.
 * User: Leghrib
 * Date: 24/04/2016
 * Time: 00:05
 *
 * @return PDOConnection
 */
function getAccessDBConnexion()
{

    //path to database file
    $database_path = getenv("pathMDB");

    //check file exist before we proceed
    if (!file_exists($database_path)) {
        PPPP::imprimer("Access database file not found !");
        die("Access database file not found !");
    }
    //create a new PDO object
    $database = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; charset=Windows-1256; DBQ=" . $database_path);


    return $database;
}


function getAccessDB_tempsConnexion()
{

    //path to database file
    $database_path = getenv("pathMDBTemp");

    //check file exist before we proceed
    if (!file_exists($database_path)) {
        PPPP::imprimer("Access database file not found !");
        die("Access database file not found !");
    }

    //create a new PDO object
    $database = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; charset=Windows-1256; DBQ=" . $database_path);


    return $database;
}
