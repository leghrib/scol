<?php
/**
 * Created by PhpStorm.
 * User: Leghrib
 * Date: 24/01/2018
 * Time: 15:40
 */

//http://localhost/gs/ws/note/note_class_mod.php?spec=204&mod=257&classe=2
require_once __DIR__ . "/../../app_utils/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

verifyConnection();

// $spec = $_GET['spec'];
$mod = $_GET['mod'];
$classe = $_GET['classe'];


$conn = getAccessDBConnexion();
$sql = "select etud.id_etud,etud.dt_nais,etud.nom,etud.prenom , notepv.* from notepv,etud,insc , notepv inner join etud on etud.id_etud=notepv.id_etud , etud  inner join insc on insc.id_etud=etud.id_etud where insc.classe=$classe and notepv.id_mod=$mod";


$stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$stmt->execute();
$num = $stmt->fetchColumn();

if ($num > 0) {
    http_response_code(200);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode(UTILS::utf8ize($result));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "لا يوجد")
    );
}



?>


