<?php

require_once __DIR__ . "/../../app_utils/autoload.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


if (isset($_GET["id_etud"])) {
    $id_etud = $_GET['id_etud'];

    $sql = "select etud.* from etud where etud.id_etud=$id_etud";
} else {
    $sql = "select etud.* from etud where true ";
}

$conn = getAccessDBConnexion();
$stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$stmt->execute();
$num = $stmt->fetchColumn();


if ($num > 0) {


    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode(UTILS::utf8ize($result));
    http_response_code(200);


} else {


    http_response_code(404);
    echo json_encode(
        array("message" => "لا يوجد !")
    );

}