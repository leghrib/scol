<?php

require_once __DIR__ . "/../../app_utils/autoload.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//verifyConnection();
$idOBS = null;
$num_obs = null;
if (isset($_GET["idObs"])) {
    $idOBS = $_GET["idObs"];

}
if (isset($_GET["numObs"])) {
    $num_obs = $_GET["numObs"];
}

$result = getObs($num_obs, $idOBS);

if ($result == null) {
    echo json_encode(
        array("message" => "لا يوجد")
    );
} else {
    echo json_encode(UTILS::utf8ize($result));

}


//get the observation text
//params : numero observation 1 ou 2 ,  id observation
//idOBS==0 getALL
function getObs($num_obs, $idOBS)
{
    try {

        if ($idOBS != null && $idOBS > 0) {
            $condition = " and  id_obs = $idOBS";
        } elseif ($idOBS == 0) {
            $condition = "";
        } else {
            return null;
        }
        $table = "";
        if ($num_obs == 1) {
            $table = "obs";
        } elseif ($num_obs == 2) {
            $table = "obs1";

        } else {
            return null;
        }

        $connT = getAccessDBConnexion();
        $sqlT = "select * from  $table  where true $condition";

        $stmtT = $connT->prepare($sqlT, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtT->execute();
        $resultT = $stmtT->fetchAll();

        return $resultT;

    } catch (Exception $e) {
        return null;
    }
}
