<?php
/**
 * Created by PhpStorm.
 * User: LEGHRIB
 * Date: 05/11/2018
 * Time: 22:13
 */
require_once __DIR__ . "/../../app_utils/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("HttpOnly: true");

if (isset($_POST['logout'])) {
    logout();
    http_response_code(200);
    $message="تم تسجيل الخروج";
    echo json_encode(array("message" => $message));
    die(0);
}

if (isset($_POST['check'])) {
    if (checkToken()) {
        http_response_code(200);
        $message="تم تسجيل الدخول مسبقا";
    } else {
        http_response_code(400);
        $message="يرجى تسجيل الدخول";
    }
    echo json_encode(array("message" => $message));
    die(0);
}
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    
    $sql = "select prof.* from prof  where tel=:tel ";
    $conn = getAccessDBConnexion();
    $stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute(array(':tel' => ($username)));
    $result = $stmt->fetchAll();

    $user = UTILS::utf8ize($result[0]);
    if (isset($user) && $user !=null) {
        http_response_code(200);

        $token = array(
                "iss" => $iss,
                "aud" => $aud,
                "iat" => $iat,
                "nbf" => $nbf,
                "exp" => $exp,
                "data"=> array(
                    "id"=>$user['id_prof']
                )
                );
        $access_token = JWT::encode($token, $key);
        setcookie('access_token', $access_token, $exp, "/");
        echo json_encode(
            array(
                "message" => "تم تسجيل الدخول"
            )
        ); 
    } else {
        http_response_code(401);

        echo json_encode(array("message" => "فشل تسجيل الدخول"));
    }
}
