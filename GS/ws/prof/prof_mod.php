<?php

require_once __DIR__ . "/../../app_utils/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

verifyConnection();

$user_id = getUserId();
$condition = "where prof.id_prof =" . $user_id;

$conn = getAccessDBConnexion();
$sql = "select spec.an_etud , spec.id_spec,spec.des_spec , spec.abr_spec , aff_ens.classe , modu.id_mod 
        from prof,aff_ens,modu,spec, prof 
        inner join aff_ens on prof.id_prof = aff_ens.id_prof, modu 
        inner join aff_ens on modu.id_mod = aff_ens.id_mod, spec 
        inner join modu on spec.id_spec = modu.id_spec 
        " . $condition;

$stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$stmt->execute();
$num = $stmt->fetchColumn();
// check if more than 0 record found
if ($num > 0) {
    http_response_code(200);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode(UTILS::utf8ize($result));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "لا يوجد")
    );
}
