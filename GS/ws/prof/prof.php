<?php

require_once __DIR__ . "/../../app_utils/autoload.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

verifyConnection();
if (isset($_GET["all"])) {
    $sql = "select * from prof";
} else
    if (isset($_GET["id"])) {
        $sql = "select * from prof where prof.id_prof=" . $_GET["id"];
    } else if (isset($_GET["tel"])) {
        $sql = "select * from prof where prof.tel=" . $_GET["tel"];
    } else {
        $user_id = getUserId();
        $sql = "select * from prof where prof.id_prof=" . getUserId();
    }

$conn = getAccessDBConnexion();
$stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$stmt->execute();
$result = $stmt->fetchAll();
echo json_encode(UTILS::utf8ize($result[0]));