<?php

require_once __DIR__ . "/../app_utils/autoload.php";
use \Firebase\JWT\JWT;

function checkToken()
{
    if (isset($_COOKIE['access_token']) && $_COOKIE['access_token'] !== null) {
        $idProf = getUserId();
    
        $sql = "select prof.* from prof  where id_prof=:idProf ";
        $conn = getAccessDBConnexion();
        $stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute(array(':idProf' => ($idProf)));
        $result = $stmt->fetchAll();
        if (isset($result[0]) && $result[0] !=null) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function getCurrentToken()
{
    return isset($_COOKIE['access_token']) ? $_COOKIE['access_token'] : null;
}

function verifyConnection()
{
    if (!checkToken()) {
        http_response_code(400);
        $message="يرجى تسجيل الدخول";
        
        echo json_encode(array("message" => $message));
        exit(0);
    }
}

function logout()
{
    $cookie_name = 'access_token';
    unset($_COOKIE[$cookie_name]);
    $res = setcookie($cookie_name, '', time() - 3600, "/");
    return $res;
}

function getUserId()
{
    global $key;
    $decoded = JWT::decode(getCurrentToken(), $key, array('HS256'));
    $data;
    if ($decoded != null) {
        $data = (array) $decoded;
    }
    $id = null;
    if (isset($data['data']->id)) {
        $id = $data['data']->id;
    }
    return $id;
}

function getUserData()
{
    global $key;
    $decoded = JWT::decode(getCurrentToken(), $key, array('HS256'));
    $data = null;
    if ($decoded != null) {
        $data = (array) $decoded;
    }
    return $data['data'];
}
