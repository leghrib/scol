<?php
/**
 * Created by PhpStorm.
 * User: Leghrib
 * Date: 27/04/2016
 * Time: 14:05
 */
require_once __DIR__ . "/../db/connectionDB.php";

class UTILS
{
    public static function convUTF8($str)
    {
        return iconv("CP1256", "UTF-8", $str);
    }

    public static function convCP1256($str)
    {
        return iconv("UTF-8", "CP1256", $str);
    }

    public static function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }


    public static function getLastGeneratedID($table, $colonneID)
    {
        $conn = getConnection();
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT max(" . $colonneID . ") as a  FROM " . $table;
        $result = $conn->query($sql);
        $list = array();
        if ($result->num_rows > 0) {
            // output data of each row
            if ($row = $result->fetch_assoc()) {
                $generatedID = $row["a"];
                return $generatedID;
            }
        } else {
        }
        $conn->close();
    }


    public static function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = UTILS::utf8ize($v);
            }
        } elseif (is_string($d)) {
            return UTILS::convUTF8($d);
        }
        return $d;
    }


}

